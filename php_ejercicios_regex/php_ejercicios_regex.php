<?php

//Realizar una expresión regular que detecte emails correctos.
$email="fulanito123@gmail.com.mx";

$expEmail="/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})/";


 if (preg_match($expEmail,$email)) {
    
     print "<p>la cadena $email es un email</p>";
 }else {
    echo  "$email no es un email";
 }

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$curp= "BAVC840614HCHRLR04";
$expCURP= "/^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM](AS|BC|BS|CC|CL|CM|CS|CH|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9A-Z]\d)$/i";

if (preg_match($expCURP,$curp)) {
    
    print "<p>la cadena $curp es un curp</p>";
}else {
   echo  "$curp no es un curp";
}

//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$palabra="Estaesunapalabrademasdecincuentaletrasexpesiondaunspacio"; 

$expPalabra= "/^((.)[a-z]{50,})$/i";

if (preg_match($expPalabra,$palabra)) {
    
    echo  "La cadena $palabra <h3 style='color:red;'> es una palabra de mas de 50 caracteres </h3>";
}
else {
   echo  "La cadena $palabra <h3 style='color:red;'> Tiene menos de 50 caracteres, tiene numeros o es mas de una palabra </h3>";
}


//Crea una funcion para escapar los simbolos especiales.
$cadenaEspecial = "Hola!+Mi'Nombre €Es&Ju%an";
$cadenaEspecial = preg_replace('([^A-Za-z0-9 ])', ' ', $cadenaEspecial);

echo  " El texto sin caracteres especiales  es : <h3 style='color:red;'>  $cadenaEspecial </h3>";



//Crear una expresion regular para detectar números decimales.
$decimal= 10.50;

$expDecimal= "/(^[0-9]{1,10}\.[0-9]{1,6}$)/";

if (preg_match($expDecimal,$decimal)) {
  
    echo  "El número $decimal <h3 style='color:red;'> es decimal </h3>";
} else {
    echo  "El número $decimal <h3 style='color:red;'> NO es decimal </h3>";
}



?>