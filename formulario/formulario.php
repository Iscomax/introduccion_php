
<?php
    session_start();
    if (!isset($_SESSION["login"])) {
        header("Location:login.php");
    }
    
    
    $varsesion = $_SESSION['login'];
    
    if ($varsesion == null || $varsesion == false) {
        header("Location:login.php");
    }



                   
                    
                    
                    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="#">JC</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cerrar_session.php">Cerrar Sesión</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="info.php">Panel </a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="row ">
            <div class="col-12">
                <form action="info.php" method="post">
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Número de cuenta</label>
                        <div class="col-sm-10">
                            <input type="number" name = "cuenta"  class="form-control" id="inputEmail3" placeholder="Número de cuenta">
                        </div>
                    </div>
                  
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text"  name ="nombre" class="form-control" id="" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Primer Apellido</label>
                        <div class="col-sm-10">
                            <input type="text"  name="paterno" class="form-control" id="" placeholder="Primer Apellido">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Segundo Apellido</label>
                        <div class="col-sm-10">
                            <input type="text"  name="materno" class="form-control" id="" placeholder="Segundo Apellido">
                        </div>
                    </div>
                   
                
                    <fieldset class="form-group">
                        <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Genero</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="gridRadios1" value="hombre">
                                    <label class="form-check-label" for="gridRadios1">
                                        Hombre
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="gridRadios2" value="mujer">
                                    <label class="form-check-label" for="gridRadios2">
                                        Mujer
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="genero" id="gridRadios3" value="otro" >
                                    <label class="form-check-label" for="gridRadios3">
                                        Otro
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="" class="col-sm-2 col-form-label">Fecha de Nacimiento</label>
                        <div class="col-sm-10">
                            <input type="date"  name = "nacimiento" class="form-control" id="" value="<?php echo date("d-m-Y");  ?>" placeholder="dd/mm/aaaa">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password"  name = "contraseña" class="form-control" id="inputPassword3" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" name="submit" class="btn btn-primary">Registrar</button>
                        </div>
                    </div>
                    
                    
                </form>
            </div>

        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>