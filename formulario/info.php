<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location:login.php");
}


$varsesion = $_SESSION['login'];

if ($varsesion == null || $varsesion == false) {
    header("Location:login.php");
}



if ($_POST) {
    //recibe de formulario.php
    $cuenta = $_POST["cuenta"];
    $contraseña = $_POST["contraseña"];
    $nombre = $_POST["nombre"];
    $paterno = $_POST["paterno"];
    $materno = $_POST["materno"];
    $nacimiento =new DateTime($_POST["nacimiento"]);
  
    $genero = $_POST["genero"];
   
    $alumnob = new \stdClass();
    
    $alumnob->cuenta = $cuenta;
    $alumnob->contraseña = $contraseña;
    $alumnob->nombre = $nombre;
    $alumnob->paterno = $paterno;
    $alumnob->materno = $materno;
    $alumnob->nacimiento = $nacimiento->format('d-m-Y');
    $alumnob->genero = $genero;
    array_push($_SESSION["DB"], $alumnob);


  
    //echo var_dump($arreglo_alumnos);
}



$cuentaADM = $_SESSION["usuario"]["cuenta"];

$nacimientoADM = $_SESSION["alumno"]["nacimiento"];



?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="#">JC</a>

            <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cerrar_session.php">Cerrar Sesión</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="formulario.php">Registrar </a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="row">
            <div class="col-12">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top img-fluid" src="avatar.png" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">Usuario Autenticado</h5>
                        <p class="card-text"></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            Numero de cuenta: <?php echo $cuentaADM;  ?>
                        </li>
                        <li class="list-group-item">Fecha Nac: <?php echo $nacimientoADM;  
                                                                ?></li>

                    </ul>
                    <div class="card-body">
                        <!--<a href="#" class="card-link">Card link</a>
                        <a href="#" class="card-link">Another link</a>-->
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <?php
                    echo '<table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Numero de cuenta</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Fecha Nacimiento</th>
                                <th scope="col">Genero</th>
                            </tr>
                        </thead>';
                    foreach ($_SESSION["DB"] as $alum) {
                    ?>
                        <tr>
                            <th scope="row"></th>
                            <td><?php echo $alum->cuenta; ?></td>
                            <td><?php echo $alum->nombre . " " . $alum->paterno . " " . $alum->materno; ?></td>
                            <td><?php echo $alum->nacimiento; ?></td>
                            <td><?php echo $alum->genero; ?></td>
                        </tr>
                    <?php
                    }
                    echo ' </table>'
                    ?>
                </div>
            </div>
        </div>

           <?php
    
           
           
           ?>         



        <div class="row">
            <div class="col-12">
                <a href="formulario.php">
                    <button type="submit" name="submit" class="btn btn-primary">Registrar</button>
                </a>
                <a href="cerrar_session.php">
                    <button type="submit" name="submit" class="btn btn-primary">Cerrar Sesion</button>
                </a>
            </div>
        </div>

    </div>
    <?PHP

     
    
    
    ?>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</body>

</html>